import "./CurrentMonthButton.css";

// Go to current month
function CurrentMonthButton() {
  return (
    <>
      <button className="CurrentMonthButton">Current</button>
    </>
  );
}

export default CurrentMonthButton;
