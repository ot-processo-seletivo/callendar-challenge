import "./ReminderDelete.css";

function ReminderDelete() {
  return (
    <>
      <button className="ReminderDeleteButton">Delete</button>
    </>
  );
}

export default ReminderDelete;
