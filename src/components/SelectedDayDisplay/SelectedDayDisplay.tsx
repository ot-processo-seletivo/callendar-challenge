import "./SelectedDayDisplay.css";

function SelectedDayDisplay() {
  return (
    <>
      <button className="CurrentSelectedDay">
        5 de Abril de 2024
      </button>
    </>
  );
}

export default SelectedDayDisplay;
