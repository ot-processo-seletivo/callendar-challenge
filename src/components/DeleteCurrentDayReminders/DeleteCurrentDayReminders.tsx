import "./DeleteCurrentDayReminders.css";

function ReminderDeleteByDate() {
  return (
    <>
      <button className="DeleteCDRButton">Delete All Reminders</button>
    </>
  );
}

export default ReminderDeleteByDate;
